# Kenken Puzzle Sover

> Same kenken puzzle online, need read a input file using *read_puzzle*()  function to read a file which contain the kenken board!

> > there is example for kenken board:
> >
> > puzzle1 = Puzzle(4, [['a','b','b','c'],
> >
> > ​           ['a','d','e','e'],
> >
> > ​           ['f','d','g','g'],
> >
> > ​           ['f','h','i','i']],
> >
> > ​          [['a',6,'*'],
> >
> > ​           ['b',3,'-'],
> >
> > ​           ['c',3,'='],
> >
> > ​           ['d',5,'+'],
> >
> > ​           ['e',3,'-'],
> >
> > ​           ['f',3,'-'],
> >
> > ​           ['g',2,'/'],
> >
> > ​           ['h',4,'='],
> >
> > ​           ['i',1,'-']])
> >
> > 
>
> Fields:
>
> ​    size (Nat)
>
> ​    board (Board)
>
> ​    constraints (listof Constraint)
>
> ​    Requires:
>
> ​     size > 0
>
> ​     len(board) == size
>
> ​     If board[i][j] is a Guess, board[i][j].symbol is a cage in the puzzle.
>
> ​     There is a one to one correspondence between the cages in board and
>
> ​      constraints. For example, if "a" represents a cage in the puzzle,
>
> ​      it appears in board and there is exactly one constraint for "a".
>
> ​     If constraints[i][2] is "=", then the cage constraints[i][0]
>
> ​      appears exactly once in the puzzle.
>
> ​     If constraints[i][2] is "/" or "-", then the cage constraints[i][0]
>
> ​      appears exactly twice in the puzzle.
>
> 

> After call *solve_kenken(file)* will return a right answer or a wrong board!
>
> 

